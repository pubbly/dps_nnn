/**
 * Created by otteran on 11/21/2014.
 */

     $(document).ready(function(){
         /*First call to load notes in the database*/
             $.ajax({
                 type:"POST",
                 url:"ajax/update_note.php",
                 dataType:"html",
                 success: function(data){
                     console.log(data);
                     $('#notitie-container').html(data);

                 }
             });

         /*Reset notitie textarea*/
         $('.voeg-toe').live('click',function(){
             $('#text').val('');

             $('#notitie').css('-webkit-transform','translateY(0%)');

             /*Save and close notitie panel*/
             $('#save-btn').live('click',function(e){
                 var data = {
                     'body1'		: $('.pr-body').val()
                 };
                 $.ajax({
                     type: "POST",
                     url: "ajax/post.php",
                     data: data,
                     success: function(msg){
                         console.log(msg);
                         $(msg).prependTo('#notitie-container');
                         location.reload(true);
                         //alert('notitie opgeslaag');

                         /*var note_template = Handlebars.compile($('#note-template').html());
                         $('#notitie-container').prepend(note_template(msg));
                         var tmp = $('.sidebar-inner-divs').data();
                         tmp.find('p').append(body);*/
                     }
                 });
                 $('#text').val('');
                 $('#notitie').css('-webkit-transform','translateY(-110%)' );
             })
         });

         /*pull panel */
         $('.bewerk-button').live('click',function(){

             var this_btn = $(this),
                 main_div = $(this_btn).parent(),
                 p = $(main_div).find('p'),
                 text_area = $('#note-body-edit');

             var p_value = $(main_div).find('p').text();
                 $(text_area).val(p_value);

             $('#bewerk').css('-webkit-transform','translateY(0%)');

             /*Delete notitie*/
             $('a.notitie-panel-del-btn').click(function(){
                 var id = $(p).attr('data-note-id');

                 $.ajax({
                     type: 'POST',
                     url: 'ajax/delete.php',
                     data: {id : id},
                     success: function() {
                         $(main_div).remove();
                     }
                 });

                 $('#bewerk').css('-webkit-transform','translateY(-110%)');
             });

             /*Save and close notitie*/
             $('.notitie-panel-bewerk-btn').click(function () {

                 var edit_text = $('#note-body-edit').val();

                 var the_data = {
                     id : $(p).attr('data-note-id'),
                     'edit' : $('#note-body-edit').val()
                 };
                 $.ajax({
                     type: "POST",
                     url: "ajax/edit.php",
                     data: the_data,
                     success: function(msg){
                        location.reload(true);
                        // alert('notite bijgewerkt');
                       var temp =  $(p).attr('data-note-id');
                       var edited =  $('#notitie-container').find("[data-note-id = '" + temp + "']");
                           edited.text(edit_text);
                     }
                 });
                 $('#bewerk').css('-webkit-transform','translateY(-110%)');
             });

         });

     });

