/**
 * Created by otteran on 11/22/2014.
 */
function init() {
    adobeDPS.libraryService.updateLibrary().completedSignal.addOnce(updateLibraryHandler, this);
}
$(document).ready(function() {


    adobeDPS.initializationComplete.addOnce(init, this);


    $('a.home-index').hide();

    $("a.panel").click(function(){
        resizeNav();
    });

    //search folios
    $('#search-btn').on('click', function(){



        var search_item = $('input#search-input').val();
        if(search_item === ""){
            alert("stel eerst een vraag...")
        }
        else{
            $('div#search-option-container').remove();
            $.get( "db/feed.json?callback=?", function( data ) {

                console.log(data);
                var item = $.grep(data.issues, function(n, i){
                    return (n.title.indexOf(search_item) > -1 || n.description.indexOf(search_item) > -1)
                });

                var issues = {"issues":item};
                var template = Handlebars.compile($('#entry-template').html());
                $('#result-data').empty();
                $('#result-data').append(template(issues));
                $('#result-data').find('h4').append(search_item);

                $(".entry").each(function(){
                    var product_id = $(this).data('productId'),
                        folio = adobeDPS.libraryService.folioMap.getByProductId(product_id),
                        label,
                        action;

                    switch(folio.state){
                        case adobeDPS.libraryService.folioStates.INVALID:
                        case adobeDPS.libraryService.folioStates.UNAVAILABLE:

                            break;
                        case adobeDPS.libraryService.folioStates.ENTITLED:
                            label = 'Download';
                            action = function(){
                                folio.download();
                            };

                            break;
                        case adobeDPS.libraryService.folioStates.DOWNLOADING:
                            if (!this.currentDownloadTransaction || (this.currentDownloadTransaction && this.currentDownloadTransaction.progress == 0)) {
                                this.setDownloadPercent(0);
                                state = "Waiting";
                            }
                            label = 'Bekijk';
                            break;
                        case adobeDPS.libraryService.folioStates.PURCHASABLE:

                            break;
                        case adobeDPS.libraryService.folioStates.PURCHASING:
                        case adobeDPS.libraryService.folioStates.EXTRACTING:
                        case adobeDPS.libraryService.folioStates.EXTRACTABLE:

                            break;
                        case adobeDPS.libraryService.folioStates.INSTALLED:
                            label = "Bekijk";
                            action = function(){
                                folio.view();
                            };
                            break;
                    }
                    $(this).find('.download').text(label).on('click', action);
                });
                console.log(item);

            });
        }


    });

    $('#link-folder').on('click', function(){

        $.get( "db/feed.json?callback=?", function( data ) {

            console.log(data);
           var folio_items = $.grep(data.issues, function(n, i){
               var folio = adobeDPS.libraryService.folioMap.getByProductId(n.productId);
               return (folio.state === 1000);
            });
           console.log(folio_items);

            var filtered_folios = {"issues" : folio_items};


            var folio_template = Handlebars.compile($('#folio-template').html());
            $('.scroller-inner').empty();
            $('.scroller-inner').append(folio_template(filtered_folios));

            $('.scroller .image').on('click', function (e){
                var folio_id = $(this).data('productId');

                $.get("db/note.json?callback=?", function (data){
                    console.log(data);
                   var note = $.grep(data.notes, function(data){
                        return(data)
                    });

                    var notes = {"notes" : note};
                    var big_template = Handlebars.compile($('#big-template').html());
                    var folio = adobeDPS.libraryService.folioMap.getByProductId(folio_id);

                    $('.big-container').append(big_template(notes));
                    console.log($(this));
                    $('.open-folio').on('click', function(){
                        console.log(folio);
                        folio.view();
                    });

                    $('img.thumb').removeClass('thumb', 1000).addClass('thumb-small', 1000);
                    $('div.footer-1').remove();
                    $('h4').remove();
                });


            });

        });
    });


     //paste and search for autoverzekering
    $('#auto').click(function(e){
        $('#search-input').val('Autoverzekering');
        setTimeout(function(){
            $('#search-btn').trigger('click')
        });
        e.preventDefault();
    });

    //paste and search for reisverzekering
    $('#reis').click(function(e){
        $('#search-input').val('Reisverzekering');
        setTimeout(function(){
            $('#search-btn').trigger('click')
        });
        e.preventDefault();
    });

    //paste and search for woonverzekering
    $('#woon').click(function(e){
        $('#search-input').val('Woonverzekering');
        setTimeout(function(){
            $('#search-btn').trigger('click')
        });
        e.preventDefault();
    });

    $("button.submit").click(function(){
        resizeNav();
    });

    $('.vraag').click(function(){
        $('vraag-full-een').slideToggle('slow')
    });

//	toggle of preview divs
    $(function() {
        var $resLinks = $('.scroller li'),
            $res = $('.big');

        $res.hide();
        $resLinks.click(function() {
            var $elm = $(this),
                childId = $elm.data('child');

            // Hide all (only one is visible, though)
            $res.hide();

            // Reset and set .current class on links
            $resLinks.removeClass('current');
            $elm.addClass('current');

            // Show related
            $('#' + childId).show('milliseconds');

            return false;
        });
    });
//end---------------------------------

//tabs for FAQs
    $('dd').hide()
    $('dt').click(function () {
        var par = $(this).next();
        $('dd').each(function () {
            if ($(this) !== par) {
                $(this).hide('fast');
            }
        });
        if (par.is(':visible')) {
            par.hide('fast');
        } else {
            par.show('fast');
            $(this).toggleClass('open');
        }
    });
//end----------------------------------------------
//tabs question main
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })
});
//end----------------------------------------------


function slideUp(){
    $("#item2").slideToggle("slow");
}
//navigation menu
function resizeNav(){

    $('div.circle-icon').removeClass('circle-icon').addClass('circle-icon-resize');
    $('nav.nav-container').removeClass('nav-container').addClass('nav-container-onclick');
    $('a.badge').removeClass('badge');
    $('p.p').remove();
    $('img.img-resize').remove();
    $('img.img-logo').remove();
    $('img.border').remove();
    $('header').remove();
    $('div').removeClass("bottom-menu");
    $('a.home-index').show('slow');

}
//end--------------------------------------------
